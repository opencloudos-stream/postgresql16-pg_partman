%global sname pg_partman
%ifarch loongarch64
%global llvm 0
%else
%global llvm 1
%endif
%global pgmajorversion 16

Summary:    A PostgreSQL extension to manage partitioned tables by time or ID
Name:       postgresql%{pgmajorversion}-%{sname}
Version:    5.1.0
Release:    3%{?dist}
License:    PostgreSQL
URL:        https://github.com/pgpartman/%{sname}
Source0:    https://github.com/pgpartman/%{sname}/archive/v%{version}.tar.gz

BuildRequires:	postgresql%{pgmajorversion}-private-devel
# pg_config -> pg_server_config (postgresql-server-devel provides)
BuildRequires:	postgresql%{pgmajorversion}-server-devel
Requires:	postgresql%{pgmajorversion}-server
# python3-psycopg2 requires libpq
Requires:	python3-psycopg2
Obsoletes:	%{sname}%{pgmajorversion} < 4.4.0-2

%description
pg_partman is a PostgreSQL extension to manage partitioned tables by time or ID.

%if %llvm
%package llvmjit
Summary:	Just-in-time compilation support for pg_partman
Requires:	%{name} = %{version}-%{release}
Requires:	llvm => 13.0

%description llvmjit
This packages provides JIT support for pg_partman
%endif

%prep
%autosetup -n %{sname}-%{version}

%build
# Change Python path in the scripts:
find . -iname "*.py" -exec sed -i "s/\/usr\/bin\/env python/\/usr\/bin\/python3/g" {} \;

USE_PGXS=1 %make_build

%install
USE_PGXS=1 %make_install

%files
%defattr(644,root,root,755)
%doc %{_docdir}/pgsql/extension/%{sname}.md
%doc %{_docdir}/pgsql/extension/*.md
%{_bindir}/check_unique_constraint.py
%{_bindir}/dump_partition.py
%{_bindir}/vacuum_maintenance.py
%{_libdir}/pgsql/%{sname}_bgw.so
%{_datadir}/pgsql/extension/%{sname}*.sql
%{_datadir}/pgsql/extension/%{sname}.control

%if %llvm
%files llvmjit
   %{_libdir}/pgsql/bitcode/src/pg_partman_bgw.index.bc
   %{_libdir}/pgsql/bitcode/src/pg_partman_bgw/src/pg_partman_bgw.bc
%endif

%changelog
* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.1.0-3
- Rebuilt for loongarch release

* Wed Jun 12 2024 Zhai Liangliang <zhailiangliang@loongson.cn> - 5.1.0-2
- [Type] other
- [DESC] Fix build error for loongarch64

* Fri Apr 12 2024 Wang Guodong <gordonwwang@tencent.com> - 5.1.0-1
- init build
